// -------   Mail Send ajax

$(document).ready(function () {
    var contactForm = $('#contact-form'); // contact form
    var contactSubmit = $('.submit-btn'); // submit button
    var alert = $('.alert-msg'); // alert div for show alert message

    // form submit event
    contactForm.on('submit', function (e) {
        e.preventDefault(); // prevent default form submit
        console.log(contactForm.serialize());

        $.ajax({
            url: 'mail.php', // form action url
            type: 'POST', // form submit method get/post
            dataType: 'html', // request type html/json/xml
            data: contactForm.serialize(), // serialize form data
            beforeSend: function () {
                alert.fadeOut();
                contactSubmit.html('Отправляется....'); // change submit button text
            },
            success: function (data) {
                if (data === 'undifined' && data.status !== 'success') {
                    window.alert(data.status);
                }
                contactForm.trigger('reset'); // reset form
                contactSubmit.html('Отправить сообщение'); // change submit button text
                //submit.attr("style", "display: none !important"); // reset submit button text
            },
            error: function (e) {
                console.log(e)
            }
        });
    });

});